connect_mcs
===

A simple command line tool for reading data from the Multi-Channel Scaler SR430 and doing some remote control to it.

## Usage

At first you will want to list connected Serial Port devices with the subcommand `list`:

```bash
connect_mcs list
>>> [SerialPortInfo { port_name: "/dev/ttyUSB0", port_type: UsbPort(UsbPortInfo {
    vid: 1659,pid: 8963, serial_number: None, manufacturer:
    Some("Prolific_Technology_Inc."), product: Some("USB-Serial_Controller") }) }]
```

In this case there is only one device named "/dev/ttyUSB0". In order to read measurement data from this device and store it in a file ~/out.dat, simply type

```bash
connect_mcs read -o ~/out.dat /dev/ttyUSB0
```

connect_mcs will not override the file if it exists, but you can tell it to do so by passing the `-f` flag.

If you are tired of spinning the wheel of the MCS, you can also do some remote control with the `set` subcommand:

```bash
connect_mcs set --bins 4096 --binwidth 40ns -l 14.0 --records 8192 /dev/ttyUSB0
```


## License

Licensed under the MIT License ([LICENSE](LICENSE) or https://opensource.org/licenses/MIT )
