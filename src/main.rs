/* connect_mcs
 * Copyright 2018 Manuel Simon
 * 
 * Licensed under the MIT License
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE. 
 * */

use std::path::PathBuf;
use std::io::prelude::*;
use std::io::BufWriter;
use std::fs::OpenOptions;
use std::time::Duration;
use std::thread;
use std::fmt;
use std::str::FromStr;


use serialport;
use serialport::{SerialPort, SerialPortSettings, FlowControl};

use structopt::StructOpt;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum BinWidth {
    Five = 0,
    Fourty = 1,
    Eighty = 2,
    Onehundretandsixty = 3,
    Threehundretandtwenty = 4,
    Sixhundretandfourty = 5,
    Onethousandtwohundretandeighty = 6,
    Twothousandfivehundretandsixty = 7,
    Fivethousandonehundretandtwenty = 8,
    Tenthousandtwohundretandfourty = 9,
    Twentythousandfourhundretandeighty = 10,
    Fourtythousandninehundretandsixty = 11,
    Eightyonethousandninehundretandtwenty = 12,
}

impl fmt::Display for BinWidth {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", *self as u8)
    }
}

#[derive(Debug)]
pub struct BinWidthError {
    value: String,
}

impl fmt::Display for BinWidthError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, r#""{}" is not a valid value for BinWidth 
Valid values are:
5ns
40ns
80ns
160ns
320ns
640ns
1.28μs, 1280ns
2.56μs, 2560ns
5.12μs, 5120ns
10.24μs, 10240ns
20.48μs, 20480ns
40.96μs, 40960ns
81.92μs, 81920ns"#, self.value)
    }
}

impl BinWidthError {
    fn new(s: String) -> BinWidthError {
        BinWidthError {value: s}
    }
}

impl FromStr for BinWidth {
    type Err = BinWidthError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "5ns" => Ok(BinWidth::Five),
            "40ns" => Ok(BinWidth::Fourty),
            "80ns" => Ok(BinWidth::Eighty),
            "160ns" => Ok(BinWidth::Onehundretandsixty),
            "320ns" => Ok(BinWidth::Threehundretandtwenty),
            "640ns" => Ok(BinWidth::Sixhundretandfourty),
            "1.28μs" | "1280ns" => Ok(BinWidth::Onethousandtwohundretandeighty),
            "2.56μs" | "2560ns" => Ok(BinWidth::Twothousandfivehundretandsixty),
            "5.12μs" | "5120ns" => Ok(BinWidth::Fivethousandonehundretandtwenty),
            "10.24μs" | "10240ns" => Ok(BinWidth::Tenthousandtwohundretandfourty),
            "20.48μs" | "20480ns" => Ok(BinWidth::Twentythousandfourhundretandeighty),
            "40.96μs" | "40960ns" => Ok(BinWidth::Fourtythousandninehundretandsixty),
            "81.92μs" | "81920ns" => Ok(BinWidth::Eightyonethousandninehundretandtwenty),
            s    => Err(BinWidthError::new(String::from(s))),
        }
    }
}



#[derive(Clone, StructOpt)]
enum Cli {
    #[structopt(name = "list", about = "List available serial port devices.")]
    List,
    #[structopt(name = "set", about = "Apply settings to the MCS.")]
    Set {
        #[structopt(parse(from_os_str))]
        path: PathBuf,
        #[structopt(long, default_value = "4", help = "Number of bins per record \
        in multiples of 1024")]
        bins: u64,
        #[structopt(long, default_value = "5ns", help = "Width of one channel in ns or μs")]
        binwidth: BinWidth,
        #[structopt(long, default_value = "3072", help = "Offset of the whole record in \
        bins")]
        offset: u64,
        #[structopt(long, default_value = "16384", help = "Number of records per scan")]
        records: u64,
        #[structopt(long, default_value = "2.0", help = "Discriminator threshold \
        for trigger")]
        level_trigger: f32,
        #[structopt(short, long, default_value = "-0.012", help = "Discriminator threshold \
        for signal")]
        level_signal: f32,
        #[structopt(long, default_value = "125", help = "Timeout used when waiting for signals \
        of the MCS")]
        timeout: u64,
    },
    #[structopt(name = "read", about = "Read measurement data from the MCS \
    and store it in an output file.")]
    Read {
        #[structopt(short = "f", long = "force", help = "Override the output file \
        if it exists")]
        force: bool,
        #[structopt(parse(from_os_str), help = "Path to the serial port device to open. \
        May be retrieved by executing 'connect_mcs list'.")]
        path: PathBuf,
        #[structopt(parse(from_os_str), short, long, default_value = "out.txt",
        help = "Output file to store the data in")]
        output: PathBuf,
        #[structopt(long, default_value = "125", help = "Timeout used when waiting for signals \
        of the MCS")]
        timeout: u64,
        #[structopt(long, default_value = "500", help = "Time to wait between \
        two reads on the serial port in ms. May be an almost arbitrary number between \
        10 and 2000")]
        pause: u64,
    },
}

#[derive(Clone)]
struct CliRead {
    force: bool,
    path: PathBuf,
    output: PathBuf,
    timeout: u64,
    pause: u64,
}

#[derive(Clone)]
struct CliSet {
    path: PathBuf,
    bins: u64,
    binwidth: BinWidth,
    offset: u64,
    records: u64,
    level_trigger: f32,
    level_signal: f32,
    timeout: u64,
}

fn read_from_device(port: &mut Box<dyn SerialPort>, args: &CliRead) -> String {
    let mut buf = [0u8; 2048];
    let mut res = String::new();
    loop {
        thread::sleep(Duration::from_millis(args.pause));
        let b = port.read(&mut buf).expect("Data could not be read.");
        println!("{} bytes read", b);
        let s = String::from_utf8_lossy(&buf[..b]);
        res += &s;
        if s.ends_with('\r') || b == 0 {
            break;
        }
    }
    res
}

fn set(args: CliSet) {
    let path = args.path.clone();
    let mut settings = SerialPortSettings::default();
    settings.flow_control = FlowControl::Hardware;
    settings.timeout = Duration::from_millis(args.timeout);
    let mut port = serialport::open_with_settings(&path, &settings)
        .expect("Path could not be opened.");

    port.write(b"OUTP 0\n").expect("Could not communicate with device");
    port.write(format!("BREC {}\n", args.bins).as_bytes()).expect("Command BREC could not be sent.");
    port.write(format!("BWTH {}\n", 0u8).as_bytes()).expect("Command BWTH could not be sent.");
    port.write(format!("BOFF {}\n", args.offset).as_bytes()).expect("Command BOFF could not be sent.");
    port.write(format!("RSCN {}\n", args.records).as_bytes()).expect("Command RSCN could not be sent.");
    port.write(format!("TRLV {:E}\n", args.level_trigger).as_bytes()).expect("Command TRLV could not be sent.");
    port.write(format!("TRSL {}\n", 1u8).as_bytes()).expect("Command TRSL could not be sent.");
    port.write(format!("DCLV {:E}\n", args.level_signal).as_bytes()).expect("Command DCVL could not be sent.");
    port.write(format!("DCSL {}\n", 1u8).as_bytes()).expect("Command DCSL could not be sent.");
}

fn read(args: CliRead) {
    let path = args.path.clone();
    let mut settings = SerialPortSettings::default();
    settings.flow_control = FlowControl::Hardware;
    settings.timeout = Duration::from_millis(args.timeout);
    let mut port = serialport::open_with_settings(&path, &settings)
        .expect("Path could not be opened.");

    //Demand devices output to be on RS232
    port.write(b"OUTP 0\n").expect("Could not communicate with device");

    //Query serial poll byte
    port.write(b"*STB? 1\n").expect("Could not query serial poll byte");
    let sb = read_from_device(&mut port, &args);
    let sb = str::parse::<u8>(sb.trim())
        .expect("No valid String was given as serial poll byte");
    if sb & 0b00000010 != 0 || sb & 0b01000000 != 0 {
        panic!("Device not ready");
    }

    //Query data
    port.write(b"BINA?\n").expect("Command could not be sent.");
    let res = read_from_device(&mut port, &args);

    let mut f = BufWriter::new(
        OpenOptions::new()
            .write(true)
            .create(true)
            .create_new(args.force)
            .open(&args.output)
            .expect("File could not be opened")
    );

    f.write_all(res.as_bytes()).expect("File could not be written to.");

    println!("Successfully saved data to {:?}", args.output);
}

fn main() {
    let args = Cli::from_args();    
    match args {
        Cli::List => {
            println!("{:?}", serialport::available_ports().expect("Ports could not be queried"));
            return;
        },
        Cli::Read{force, path, output, timeout, pause} => {
            read(CliRead{
                force,
                path,
                output,
                timeout,
                pause,
            });
        }
        Cli::Set{path, bins, binwidth, offset, records, level_trigger, level_signal, timeout} => {
            set(CliSet{
            path,
            bins,
            binwidth,
            offset,
            records,
            level_trigger,
            level_signal,
            timeout,
            });
        }
    }
    
}
